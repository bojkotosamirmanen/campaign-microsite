---
---


### Bashkohu

1. Mos harxho para në asnjë prej bizneseve të Samir Manes, si p.sh. SPAR, Neptun, Jumbo, TEG, QTU, Tirana Bank, Aladini, Smart Point, Fashion Group Albania, etj!
2. Bind çdo shoqe/shok dhe të afërm për domosdoshmërinë e bojkotimit të produkteve të Samir Manes!
3. Bashkohu me aktivitetet e [Sindikatës së Minatorëve të Bashkuar të Bulqizës](https://www.facebook.com/MinatoretSMBB/)! Në pamundësi për të shkuar në Bulqizë, shpërnda në rrjetet sociale deklaratat e tyre!
4. Kontribuo financiarisht, sipas mundësive, për të ndihmuar mbijetesën e minatorëve të pushuar nga puna dhe veprimtaritë e SMBB!
5. Ushtro presionin tënd si qytetar ndaj çdo funksionari publik dhe medie që mund të ketë ndikim në vendosjen e drejtësisë për minatorët e Bulqizës!
6. Organizohu me të tjerë si vetja në vendin e punës, të shkollës apo në lagje për të diskutuar çështjet e punës dhe të demokracisë në vendin tonë.
7. Printo disa [sticker-a](https://gitlab.com/bojkotosamirmanen/materialet-e-fushates/-/tree/master/Stickera) dhe ngjiti në lagjen ku banon. Nëse ke mundësi, shpërndaji në rrjetet sociale me hashtagun #BojkotoSamirManen.
8. Ndiq faqen [Bojkoto Samir Manen në Facebook](https://www.facebook.com/bojkotosamirmanen/), njofto të njohurit për këtë faqe, dhe kontribuo në media sociale me postime #BojkotoSamirManen.

