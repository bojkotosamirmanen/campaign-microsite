---
layout: default
---


### Rreth fushatës

Ky website është krijuar nga aktivistë në kuadër të fushatës *#BojkotoSamirManen* dhe ka për qëllim të informojë qytetarët mbi shfrytëzimin dhe kërcënimet që Samir Mane po u ushtron minatorëve të Bulqizës të cilët punojnë në minierën e kompanisë AlbChrome. Deri më tani, Samir Mane ka pushuar nga puna katër punëtorë të cilët janë pjesë e këshillit të Sindikatës së Minatorëve të bashkuar të Bulqizës, dhe po kërcënon me qindra të tjerë me pushim nga puna për shkak të aktivitetit të tyre sindikal. Duke qenë se Samir Mane i ka blerë të gjitha mediat në vend, përdorimi i një website-i të tillë përbën domosdoshmëri për të treguar se çfarë po ndodh në Bulqizë.

Për më shumë kontakto:

* Email: bojkotosamirmane@protonmail.com
* Facebook: [Bojkoto Samir Manen!](https://www.facebook.com/bojkotosamirmanen/)
* Facebook: [Sindikata e Minatorëve të Bashkuar të Bulqizës](https://www.facebook.com/MinatoretSMBB/)
* Newsletter: [The Voice of United Miners of Bulqiza Trade Union](https://smbb.substack.com/)
